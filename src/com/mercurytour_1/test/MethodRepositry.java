package com.mercurytour_1.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MethodRepositry {
	static WebDriver driver;

	public static void appLunch() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
	}

	// Aarush added on bitbucket
	public static void verifyValidlogin() throws InterruptedException, AWTException, IOException {
		// WebElement uname = driver.findElement(By.name("userName"));
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		// uname.sendKeys("dasd");

		Properties obj = new Properties();
		FileInputStream objfile = new FileInputStream("./TestData/testdata.properties");
		obj.load(objfile);
		String uname1 = obj.getProperty("username");
		uname.sendKeys(uname1);

		// Now using username by Sikuli

		// Screen screen = new Screen();
		// Thread.sleep(5000);
		// Pattern username = new Pattern("./Sikuli/usernamenew.png");
		// screen.wait(username, 10);
		// screen.type(username, "dasd");

		// Thread.sleep(5000);
		// Pattern password = new Pattern("./Sikuli/Password.png");
		// screen.wait(password, 10);
		// screen.type(password, "dasd");

		// Use Action Class
		// WebElement signin = driver.findElement(By.name("login"));
		// Actions builder = new Actions(driver);
		// builder.moveToElement(signin).click().build().perform();

		// signin.click();

		WebElement pwd = driver.findElement(By.name("password"));
		// pwd.sendKeys("dasd");

		String password1 = obj.getProperty("password");
		pwd.sendKeys(password1);

		WebElement signin = driver.findElement(By.name("login"));
		signin.click();
		/*
		 * // Login by robot class Robot robotObj = new Robot();
		 * robotObj.keyPress(KeyEvent.VK_ENTER); robotObj.keyRelease(KeyEvent.VK_ENTER);
		 */

		String ExpectedTitle = "Find a Flight: Mercury Tours:";
		String ActTitle = driver.getTitle();
		if (ExpectedTitle.equals(ActTitle)) {
			System.out.println("Test case is Passed");
		} else {
			System.out.println("Test case is Failed");
		}
		Thread.sleep(3000);
	}

	// Use select class (ByVisible, ByIndex, ByValue
	public static void verfiyPassenger() throws InterruptedException {
		WebElement departing = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select dropdown = new Select(departing);
		dropdown.selectByVisibleText("London");
		Thread.sleep(1000);
		dropdown.selectByIndex(6);
		Thread.sleep(1000);
		dropdown.selectByValue("Sydney");
		Thread.sleep(1000);
	}

	public static void filsFm() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://files.fm/");
		Thread.sleep(3000);
	}

	// Use AutoIT functions
	public static void fileUpload() throws InterruptedException, IOException {
		Thread.sleep(3000);
		WebElement upload = driver.findElement(By.id("uploadifive-file_upload"));
		upload.click();
		Thread.sleep(3000);
		Runtime.getRuntime().exec("./Driver/FlieFmFileUpload.exe");
		Thread.sleep(10000);
		driver.close();
	}

	// Testinvalid testcase
	public static void inVerifyValidlogin() throws InterruptedException, AWTException, FindFailed {
		// WebElement uname = driver.findElement(By.name("userName"));
		// WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		// uname.sendKeys("dasd");

		// Now using username by Sikuli

		Screen screen = new Screen();
		Thread.sleep(5000);
		Pattern username = new Pattern("./Sikuli/usernamenew.png");
		screen.wait(username, 10);
		screen.type(username, "dasd123");

		Thread.sleep(5000);
		Pattern password = new Pattern("./Sikuli/Password.png");
		screen.wait(password, 10);
		screen.type(password, "dasd123");

		// Use Action Class
		WebElement signin = driver.findElement(By.name("login"));
		Actions builder = new Actions(driver);
		builder.moveToElement(signin).click().build().perform();

		String ExpectedTitle = "Find a Flight: Mercury Tours:";
		String ActTitle = driver.getTitle();
		if (ExpectedTitle.equals(ActTitle)) {
			System.out.println("Test case is Passed");
		} else {
			System.out.println("Test case is Failed");
		}
		Thread.sleep(3000);
	}

	public static void webTableHandling() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://money.rediff.com/gainers/bsc/daily/groupa");
		Thread.sleep(3000);
		// No.of Columns
		List col = driver.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
		System.out.println("No of cols are : " + col.size());
		// No.of rows
		List rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
		System.out.println("No of rows are : " + rows.size());

	}

	public static void findCurrentPrice() throws IOException {
		// *[@id='leftcontainer']/table/tbody/tr[1]/td[1]/a

		String before_xpath = "//*[@id='leftcontainer']/table/tbody/tr[";
		String after_xpath = "]/td[1]/a";

/////////////////////////////		
//		for (int i = 1; i <= 195; i++) {
//			String companyName = driver.findElement(By.xpath(before_xpath + i + after_xpath)).getText();
//			// System.out.println("The company name is - "+ companyName);
//
//			if (companyName.contains("Kwality")) {
//				// *[@id='leftcontainer']/table/tbody/tr[1]/td[4]
//				String currentPrice = driver
//						.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[" + i + "]/td[4]")).getText();
//				System.out.println("The today current price of " + companyName + " is - " + currentPrice);
//			}
//		}
					
		for (int i = 1; i <= 195; i++) {
			String companyName = driver.findElement(By.xpath(before_xpath + i + after_xpath)).getText();
			// System.out.println("The company name is - "+ companyName);

			Properties obj = new Properties();
			FileInputStream objfile = new FileInputStream("./TestData/testdata.properties");
			obj.load(objfile);
			String currentCompName = obj.getProperty("currentCompanyName");
			
			if (companyName.contains(currentCompName)) {
				// *[@id='leftcontainer']/table/tbody/tr[1]/td[4]
				String currentPrice = driver.findElement(By.xpath("//*[@id='leftcontainer']/table/tbody/tr[" + i + "]/td[4]")).getText();
				System.out.println("The today current price of " + companyName + " is -> " + currentPrice);
			}
		}
		
	}

	public static void browserClose() {
		driver.close();
	}

}
